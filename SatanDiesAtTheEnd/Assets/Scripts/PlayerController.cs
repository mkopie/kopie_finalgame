﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    //has to do with movement or animation
    public float moveSpeed;
    public float inMotion;
    private Rigidbody2D body;
    private Animator anim;

    //has to do with attacking
    public float timeBetweenAttacks = 0.5f;
    public bool inRange;
    float timer;
    GameObject enemy;
    GameObject enemyHealth;
    GameObject blast;
    GameObject playerCharge;
    private CapsuleCollider2D hitbox;
    float framecount;
    HealthManager healthManager;
    public int enemyValue;
    public int playerVuln;
    public int charge;
    float vulnCount;
    public string moveType;
    public bool blasting;
    public bool firstframe;
    

    // Use this for initialization
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        inMotion = 0.0f;

        enemy = GameObject.FindGameObjectWithTag("Enemy");
        enemyHealth = GameObject.Find("EnemyHealth");
        blast = GameObject.FindGameObjectWithTag("blast");
        blast.GetComponent<SpriteRenderer>().enabled = false;
        playerCharge = GameObject.Find("PlayerCharge");
        charge = playerCharge.GetComponent<PlayerCharge>().charge;

        hitbox = GetComponent<CapsuleCollider2D>();
        playerVuln = 0;
        vulnCount = 0;

        enemyValue = 40;

        hitbox.enabled = false;
        framecount = 0.0f;
        blasting = false;

        moveType = "None";
        firstframe = false;
    }

    void OnTriggerEnter2D (Collider2D other)
    {
        if(other.gameObject == enemy)
        {
            inRange = true;
            print("PLAYER DAMAGE");
        }
    }

    void OnTriggerExit2D (Collider2D other)
    {
        if (other.gameObject == enemy)
        {
            inRange = false;
        }
    }

    void TakeDamage()
    {
        enemyValue = enemyValue - 1;
        enemyHealth.GetComponent<EnemyManager>().health = enemyValue;
    }

    void ChargeDamage()
    {
        enemyValue = enemyValue - 20;
        enemyHealth.GetComponent<EnemyManager>().health = enemyValue;
    }

    // Update is called once per frame
    void Update()
    {

        //MOVEMENT RELATED

        //movement
        if ((Input.GetAxisRaw("Horizontal") > 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f) && inMotion < 0.5f)
        {
            body.velocity = new Vector2(Input.GetAxisRaw("Horizontal") * moveSpeed, body.velocity.y);
            anim.SetFloat("MoveX", Input.GetAxisRaw("Horizontal"));
        }

        //change back to no movement
        if (Input.GetAxisRaw("Horizontal") < 0.5f && Input.GetAxisRaw("Horizontal") > -0.5f)
        {
            body.velocity = new Vector2(0f, body.velocity.y);
            anim.SetFloat("MoveX", Input.GetAxisRaw("Horizontal"));
        }


        //CHARGE RELATED

        //charge animation
        if (Input.GetAxisRaw("Fire3") > 0.5f && inMotion < 0.5f)
        {
            charge = playerCharge.GetComponent<PlayerCharge>().charge;

            if (charge == 4)
            {
                anim.SetFloat("IsBlast", 1.0f);
                inMotion = 1.0f;
                blast.GetComponent<SpriteRenderer>().enabled = true;

                if (blasting == false)
                {
                    ChargeDamage();
                    blasting = true;
                    charge = 0;
                    playerCharge.GetComponent<PlayerCharge>().charge = 0;
                }
            }
            
                
        }

        //no charge
        if (Input.GetAxisRaw("Fire3") < 0.5f)
        {
            anim.SetFloat("IsBlast", 0f);
            inMotion = 0.0f;
            blast.GetComponent<SpriteRenderer>().enabled = false;
            blasting = false;
        }


        //BLOCK RELATED

        //crouch animation
        if (Input.GetAxisRaw("Vertical") < -0.5f && inMotion < 0.5f)
        {
            anim.SetFloat("IsCrouch", 1.0f);
            inMotion = 1.0f;
            playerVuln = 2;
        }

        //block animation
        if (Input.GetAxisRaw("Vertical") > 0.5f && inMotion < 0.5f)
        {
            anim.SetFloat("IsBlock", 1.0f);
            inMotion = 1.0f;
            playerVuln = 1;
        }

        //no block
        if (Input.GetAxisRaw("Vertical") < 0.5f && Input.GetAxisRaw("Vertical") > -0.5f)
        {
            anim.SetFloat("IsBlock", 0f);
            anim.SetFloat("IsCrouch", 0f);
            inMotion = 0.0f;
        }


        //ATTACK RELATED

        //punch animation
        if (Input.GetAxisRaw("Fire1") > 0.5f && inMotion < 0.5f)
        {
            anim.SetFloat("IsPunch", 1.0f);
            inMotion = 1.0f;
            hitbox.enabled = true;
            moveType = "Punch";
        }

        //kick animation
        if (Input.GetAxisRaw("Fire2") > 0.5f && inMotion < 0.5f)
        {
            anim.SetFloat("IsKick", 1.0f);
            inMotion = 1.0f;
            hitbox.enabled = true;
            moveType = "Kick";
        }

        //no punch
        if (Input.GetAxisRaw("Fire1") < 0.5f)
        {
            anim.SetFloat("IsPunch", 0f);
            inMotion = 0.0f;
        }

        //no kick
        if (Input.GetAxisRaw("Fire2") < 0.5f)
        {
            anim.SetFloat("IsKick", 0f);
            inMotion = 0.0f;
        }

        if(hitbox.enabled == true)
        {
            framecount = framecount + 1.0f;

            if(framecount >= 20.0f)
            {
                if(inRange == true)
                {
                    if(enemy.GetComponent<EnemyController>().enemyVuln == 0)
                    {
                        TakeDamage();
                        print("Take damage");
                    }
                    else if(enemy.GetComponent<EnemyController>().enemyVuln == 1 && moveType == "Kick")
                    {
                        TakeDamage();
                        print("KICK DAMAGE");
                    }
                    else if (enemy.GetComponent<EnemyController>().enemyVuln == 2 && moveType == "Punch")
                    {
                        TakeDamage();
                        print("PUNCH DAMAGE");
                    }
                }
                hitbox.enabled = false;
                framecount = 0.0f;
            }
        }

        if(playerVuln == 1 || playerVuln == 2)
        {
            vulnCount = vulnCount + 1.0f;

            if(vulnCount > 30.0f)
            {
                vulnCount = 0.0f;
                playerVuln = 0;
            }
        }
    }
}