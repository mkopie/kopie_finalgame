﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour {


    //has to do with movement or animation
    public float moveSpeed;
    public float inMotion;
    private Rigidbody2D body;
    private Animator anim;
    public int choice;
    public int timer;

    //has to do with attacking
    private CapsuleCollider2D hitbox;
    public bool inRange;
    GameObject player;
    GameObject playerHealth;
    HealthManager healthManager;
    PlayerController playerController;
    GameObject playerCharge;
    public int playerValue;
    float framecount;
    public int enemyVuln;
    public int charge;
    float vulnCount;
    public string moveType;
    public static float waittime;

    // Use this for initialization
    void Start () {
        body = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        inMotion = 0.0f;
        choice = 0;
        timer = 0;

        playerCharge = GameObject.Find("PlayerCharge");
        charge = playerCharge.GetComponent<PlayerCharge>().charge;

        hitbox = GetComponent<CapsuleCollider2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = GameObject.Find("PlayerHealth");
        playerValue = 20;
        framecount = 0.0f;
        moveType = "None";
        waittime = 80;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            inRange = true;
            print("ENEMY DAMAGE");
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject == player)
        {
            inRange = false;
        }
    }

    void TakeDamage()
    {
        playerValue = playerValue - 2;
        playerHealth.GetComponent<HealthManager>().health = playerValue;
    }

    // Update is called once per frame
    void Update () {

        charge = playerCharge.GetComponent<PlayerCharge>().charge;

        timer = timer + 1;

        if(timer == 1)
        {
            choice = Random.Range(0, 12);

            //moveleft
            if(choice == 0)
            {
                anim.Play("enemymoveleft");
                body.velocity = new Vector2(-1.0f, 0.0f);
            }
            //moveright
            else if (choice == 1)
            {
                anim.Play("enemymoveright");
                body.velocity = new Vector2(1.0f, 0.0f);
            }
            //punch
            else if (choice == 2 || choice == 8 || choice == 10 || choice == 7)
            {
                anim.Play("enemypunch");
                hitbox.enabled = true;
                moveType = "Punch";
            }
            //kick
            else if (choice == 3 || choice == 9 || choice == 11 || choice == 6)
            {
                anim.Play("enemykick");
                hitbox.enabled = true;
                moveType = "Kick";
            }
            //block
            else if (choice == 4)
            {
                anim.Play("enemyblock");
                enemyVuln = 1;
            }
            //crouch
            else if (choice == 5)
            {
                anim.Play("enemycrouch");
                enemyVuln = 2;
            }
        }
        else if(timer >= 40 && timer <= waittime)
        {
            anim.Play("enemyidle");
            hitbox.enabled = false;
            body.velocity = new Vector2(0.0f, 0.0f);
        }
        else if(timer > waittime)
        {
            timer = 0;
        }

        if (hitbox.enabled == true)
        {
            framecount = framecount + 1.0f;

            if (framecount >= 35.0f)
            {
                if (inRange == true)
                {
                    if (player.GetComponent<PlayerController>().playerVuln == 0)
                    {
                        TakeDamage();
                        print("Take damage");
                    }
                    else if (player.GetComponent<PlayerController>().playerVuln == 1 && moveType == "Kick")
                    {
                        TakeDamage();
                        print("KICK DAMAGE");
                    }
                    else if (player.GetComponent<PlayerController>().playerVuln == 2 && moveType == "Punch")
                    {
                        TakeDamage();
                        print("PUNCH DAMAGE");
                    }
                    else if (player.GetComponent<PlayerController>().playerVuln == 1 && moveType == "Punch")
                    {
                        if(charge != 4)
                        {
                            charge = charge + 1;
                            playerCharge.GetComponent<PlayerCharge>().charge = playerCharge.GetComponent<PlayerCharge>().charge + 1;
                        }
                            
                    }
                    else if (player.GetComponent<PlayerController>().playerVuln == 2 && moveType == "Kick")
                    {
                        if (charge != 4)
                        {
                            charge = charge + 1;
                            playerCharge.GetComponent<PlayerCharge>().charge = playerCharge.GetComponent<PlayerCharge>().charge + 1;
                        }
                    }
                }
                hitbox.enabled = false;
                framecount = 0.0f;
            }
        }

        if (enemyVuln == 1 || enemyVuln == 2)
        {
            vulnCount = vulnCount + 1.0f;

            if (vulnCount > 30.0f)
            {
                vulnCount = 0.0f;
                enemyVuln = 0;
            }
        }
    }
}
