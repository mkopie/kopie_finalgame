﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCharge : MonoBehaviour {

    public int charge;
    Text text;

    // Use this for initialization
    void Start () {
        text = GetComponent<Text>();
        charge = 0;
    }
	
	// Update is called once per frame
	void Update () {
        text.text = "SP: " + charge + "/4";
    }
}
