﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EnemyManager : MonoBehaviour
{
    public int health;
    Text text;
    Text dialogue;
    public Canvas wincanvas;

    // Use this for initialization
    void Start () {
        text = GetComponent<Text>();
        health = 40;
        dialogue = GameObject.Find("Dialogue").GetComponent<Text>();

        if (HealthManager.round == 1)
        {
            EnemyController.waittime = 70;
            dialogue.text = "Forneus the Deranged: GRUUUUAHHWHHAHHHHHSHHSAHHHH!!!";
        }
        else if (HealthManager.round == 2)
        {
            EnemyController.waittime = 62;
            dialogue.text = "Lahash the Traitor: Let's play a game called DEATH!";
        }
        else if (HealthManager.round == 3)
        {
            EnemyController.waittime = 52;
            dialogue.text = "Vassago the Forgotten: Even the greatest of men will fall...";
        }
        else if (HealthManager.round == 4)
        {
            EnemyController.waittime = 41;
            dialogue.text = "Satan the Banished: There is no way out. Damnation is inevitable!";
        }
        else if (HealthManager.round >= 5)
        {
            wincanvas.enabled = true;
            Time.timeScale = 0;
        }
    }

    void Reset()
    {
        HealthManager.round = HealthManager.round + 1;
        print(EnemyController.waittime);
        print(HealthManager.round);
        SceneManager.LoadScene(0);
    }
	
	// Update is called once per frame
	void Update () {
        text.text = "HP: " + health;

        if(health <= 0)
        {
            Reset();
        }
	}
}